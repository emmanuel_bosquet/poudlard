import personnes.Eleve;
import personnes.Professeur;
import personnes.DirecteurDePoudlard;
import personnes.DirecteurDeMaison;
import personnes.PrefetEnChef;
import institutions.Matiere;
import institutions.NomDeMaison;
import institutions.Poudlard;

public class Main {

    public static void main(String[] args) {

        // instancier la directrice
        DirecteurDePoudlard hermioneGranger = new DirecteurDePoudlard(false, "Hermione Granger", NomDeMaison.GRYFFONDOR, Matiere.DCFM);
        hermioneGranger.sePresente();

        // instancier poudlard avec sa directrice
        Poudlard poudlard = new Poudlard(hermioneGranger);

        // quelques profs et élèves
        Professeur londubat = new Professeur(true, "Neville Longdubas", NomDeMaison.GRYFFONDOR, Matiere.BOTANIQUE);
        londubat.sePresente();

        DirecteurDeMaison malfoy = new DirecteurDeMaison(true, "Draco Malfoy", NomDeMaison.SERPENTARD, Matiere.POTIONS);
        malfoy.sePresente();

        Eleve ezekiel = new PrefetEnChef(true, "Ezequiel Kadabra", NomDeMaison.GRYFFONDOR, 7);
        ezekiel.sePresente();

        Eleve marina = new Eleve(false, "Marina Metzger", NomDeMaison.POUFSOUFFLE, 5);
        marina.sePresente();

        Eleve emmanuel = new Eleve(true, "Emmanuel Bosquet", NomDeMaison.SERDAIGLE, 2);
        emmanuel.sePresente();

        ezekiel.modifierLeScoreDe(marina, -10, poudlard);
        poudlard.donnerLesScores();
        emmanuel.sePresente();
        londubat.donneUneRetenue(emmanuel);
        hermioneGranger.renvoie(emmanuel);
    }

}
