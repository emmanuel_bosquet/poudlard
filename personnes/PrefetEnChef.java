package personnes;

import institutions.NomDeMaison;

public class PrefetEnChef extends Eleve {

    public PrefetEnChef(boolean sex, String nom, NomDeMaison maison, int annee) {
        // on appelle d'abord le constructeur de la classe parent
        super(sex, nom, maison, annee);
        enleveMaxPoints = 50;
        ajouteMaxPoints = 50;
    }

}
