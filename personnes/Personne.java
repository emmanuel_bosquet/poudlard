package personnes;

import institutions.NomDeMaison;
import institutions.Poudlard;

public class Personne {
    // les propriétés de chaque personne
    boolean sexe;
    String nom;
    NomDeMaison maison;
    int ajouteMaxPoints;
    int enleveMaxPoints;

    // Ceci est le constructeur de la classe. Drôle de syntaxe.
    public Personne(boolean sexe, String nom, NomDeMaison maison) {
        this.sexe = sexe;
        this.nom = nom;
        this.maison = maison;
        // par défaut, une personne ne peut pas enlever ou ajouter de points
        // mais c'est tellement plus pratique de mettre cette logique-là
        // dans Personne, parce que tout le monde en hérite !
        ajouteMaxPoints = 0;
        enleveMaxPoints = 0;
    }

    // Ceci est une méthode
    public void sePresente() {
        System.out.println("Je m'appelle " + this.nom + ".");
    }

    // ajouter le score de la maison d'un élève (on passe poudlard en argument
    // car on doit pouvoir accéder à toutes les maisons)
    public void modifierLeScoreDe(Eleve eleve, int points, Poudlard poudlard) {

        // un garde-fou, si jamais un simple élève veut enlever des points à quelqu'un…
        if (ajouteMaxPoints == 0) {
            System.out.println("Je ne peux pas modifier le score de qui que ce soit !");
            return;
        }

        // ou si un préfet veut abuser de son pouvoir
        if (ajouteMaxPoints < points && points < enleveMaxPoints) {
            System.out.println("Je n'ai pas le droit de modifier le score à ce point!");
            return;
        }

        // dire que tout ça c'est compilé magiquement par la JVM… (la Javamachine
        // Véritablement Magique !)

        System.out.println("Moi, " + nom + ", j'enlève " + points + " points à " + eleve.maison + " !");

        // on itère sur le tableau "maisons" de poudlard
        // et si la maison correspond à celle de l'élève on change le score
        // entre nous, ça ressemble vachement à du javascript
        for (int i = 0; i < 4; i++) {
            if (poudlard.maisons[i].nom == eleve.maison) {
                poudlard.maisons[i].score += points;
                System.out.println("Le score de " + eleve.maison + " est maintenant de " + poudlard.maisons[i].score);
            }
        }

    }
}