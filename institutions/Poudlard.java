package institutions;

import personnes.DirecteurDePoudlard;

public class Poudlard {

    
    public Maison[] maisons;
    public DirecteurDePoudlard directeur;

    // le constructeur prend un DirecteurDePoudlard en argument
    // dans le main ce sera hermioneGranger
    public Poudlard(DirecteurDePoudlard directeur) {
        Maison[] tableauDeMaisons = new Maison[4];
        tableauDeMaisons[0] = new Maison(NomDeMaison.SERPENTARD);
        tableauDeMaisons[1] = new Maison(NomDeMaison.SERDAIGLE);
        tableauDeMaisons[2] = new Maison(NomDeMaison.POUFSOUFFLE);
        tableauDeMaisons[3] = new Maison(NomDeMaison.GRYFFONDOR);
        maisons = tableauDeMaisons;
        
        this.directeur = directeur;
    }

    public void donnerLesScores() {
        for (int i = 0; i < 4; i++) {
            maisons[i].donnerLeScore();
        }
    }
}
