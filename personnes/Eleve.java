package personnes;

import institutions.NomDeMaison;

public class Eleve extends Personne {

    // une propriété supplémentaire comparé à Personne : l'année à poudlard !
    int annee;

    public Eleve(boolean sexe, String nom, NomDeMaison maison, int annee) {
        // on appelle le constructeur parent
        super(sexe, nom, maison);
        // et on ajoute la propriété annee
        this.annee = annee;
    }
}
