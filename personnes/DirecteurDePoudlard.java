package personnes;

import institutions.NomDeMaison;
import institutions.Matiere;

// A noter que le directeur, enfin la directrice, n'est pas directrice de maison elle-même
public class DirecteurDePoudlard extends Professeur {

    public DirecteurDePoudlard(boolean sexe, String nom, NomDeMaison maison, Matiere matiere) {
        // on récupère le constructeur de Professeur
        super(sexe, nom, maison, matiere);
        ajouteMaxPoints = 200;
        enleveMaxPoints = 250;
    }

    public void renvoie(Eleve eleve) {
        System.out.println("Moi, " + nom + ", je renvoie " + eleve.nom + " de Poudlard ! Qu'il pourrisse dans les limbes !");
    }
}
