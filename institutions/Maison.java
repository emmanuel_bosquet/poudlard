package institutions;

import personnes.DirecteurDeMaison;
import personnes.Prefet;

public class Maison {

    public NomDeMaison nom;
    public DirecteurDeMaison directeur;
    public int score;
    public Prefet prefetGarcon;
    public Prefet prefetFille;

    // Le constructeur prend en argument un nom de maison
    public Maison(NomDeMaison nomDeMaison) {
        nom = nomDeMaison;
        // chaque maison commence avec un score à zéro
        score = 0;
    }

    public void donnerLeScore() {
        System.out.println(nom + " : " + score);
    }
}
