# Poudlard, en UML et… en Java

Cher correcteur,  
Voici comment je vois Poudlard, avec de l'héritage et du `public Main`.

Ah, la consigne ne précisait pas quel language !

Mais j'ai mis des commentaires dans le code, ça va aller c'est lisible.

## Diagramme de classes

![img](diagramme_de_classes.png)

## Diagramme de cas d'utilisations

![img](use_cases.png)

## Comment vérifier que le code est bon ?

Sur ubuntu, on peut installer java, compiler main, et lancer le programme.

    sudo apt install openjdk-8-jdk
    javac Main.java
    java Main

Voilà l'output du programme: 

    Je m'appelle Hermione Granger.
    Je m'appelle Neville Longdubas.
    Je m'appelle Draco Malfoy.
    Je m'appelle Ezequiel Kadabra.
    Je m'appelle Marina Metzger.
    Je m'appelle Emmanuel Bosquet.
    Moi, Ezequiel Kadabra, j'enlève -10 points à POUFSOUFFLE !
    Le score de POUFSOUFFLE est maintenant de -10
    SERPENTARD : 0
    SERDAIGLE : 0
    POUFSOUFFLE : -10
    GRYFFONDOR : 0
    Je m'appelle Emmanuel Bosquet.
    Moi, Neville Longdubas, je donne une retenue à Emmanuel Bosquet !
    Moi, Hermione Granger, je renvoie Emmanuel Bosquet de Poudlard ! Qu'il pourrisse dans les limbes !
