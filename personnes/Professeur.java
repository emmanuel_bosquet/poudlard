package personnes;

import institutions.NomDeMaison;
import institutions.Matiere;

public class Professeur extends Personne {

    Matiere matiere;

    public Professeur(boolean sexe, String nom, NomDeMaison maison, Matiere matiere) {
        super(sexe, nom, maison);
        this.matiere = matiere;
        ajouteMaxPoints = 100;
        enleveMaxPoints = 100;
    }

    public void donneUneRetenue(Eleve eleve) {
        System.out.println("Moi, " + nom + ", je donne une retenue à " + eleve.nom + " !");
    }
}
