package personnes;

import institutions.NomDeMaison;
import institutions.Matiere;

public class DirecteurDeMaison extends Professeur {

    public DirecteurDeMaison(boolean sex, String nom, NomDeMaison maison, Matiere matiere) {
        // on reprend le constructeur de Professeur, qui donne le pouvoir
        // d'ajouter 100 et -100 points maximum
        super(sex, nom, maison, matiere);
    }
}
