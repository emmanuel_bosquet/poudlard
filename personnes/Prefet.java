package personnes;

import institutions.NomDeMaison;

public class Prefet extends Eleve {

    public Prefet(boolean sexe, String nom, NomDeMaison maison, int annee) {
        // voici comment on appelle le constructeur de la classe parent !
        super(sexe, nom, maison, annee);
        enleveMaxPoints = 25;
        ajouteMaxPoints = 25;
    }

}
